<?php
error_reporting(0);
session_start();
include('init/db_connection.php');

if (isset($_SESSION['user_id']) === false && empty($_SESSION['user_id'])) {
    header('Location: index.html');
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
<style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th	{
    background-color: #5c5c5c;
    color: white;
	padding-top: 10px;
	padding-bottom: 10px;
}
#course-main-div{
	padding: 15px;
	margin-top: 30px;
	border: 1px solid gray;
	border-radius: 10px;
	background-color: #f1f1f1;
}
#course-main-div ul{
	line-height: 30px;
}
.submit-button{
	padding: 5px 10px;
	margin-top: 10px;
	font-size: 18px;
	width: 150px;
	text-align: center;
}
</style>
</head>
<body>
<h1 style="text-align: center;">Admin Page</h1>
<a href="logout.php" style="float: right;">Logout</a><br><br>
<table id="t01">
  <thead>
		<tr>
			<th>Activate</th>
			<th>Registration</th>
			<th>Firstname</th>
			<th>Lastname</th>
			<th>Birthday</th>
			<th>Gender</th>
			<th>Email</th>
			<th>Mobile</th>
			<th>Phone</th>
			<th>Time</th>
		</tr>
	</thead>
	<tbody>
		<?php
		session_start();
		include('init/db_connection.php');

		$sql = "SELECT * FROM student";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			while ($row = mysqli_fetch_assoc($result)) {
				if ($row['active'] == 0) {
					?>
					<tr style="text-align: left;">
						<td>
							<form action="activate.php?id=<?php echo $row['roll_no']; ?>" method="POST">
								<input type="submit" value="Activate">
							</form>
						</td>
						<td><?php echo $row['registration']; ?></td>
						<td><?php echo $row['firstname']; ?></td>
						<td><?php echo $row['lastname']; ?></td>
						<td><?php echo $row['birthday']; ?></td>
						<td><?php echo $row['gender']; ?></td>
						<td><?php echo $row['email']; ?></td>
						<td><?php echo $row['mobile_no']; ?></td>
						<td><?php echo $row['phone_no']; ?></td>
						<td><?php echo $row['time']; ?></td>
					</tr>
					<?php
				}
			}
		}
		?>

	</tbody>
</table>
	<?php
		include('init/db_connection.php');

		$sql1 = "SELECT * FROM courses";
		$result1 = mysqli_query($conn, $sql1);
		
		if (mysqli_num_rows($result1) > 0) {
			while ($row1 = mysqli_fetch_assoc($result1)) { ?>
			<div id="course-main-div">
			<h2 style="text-align: center;"><?php  echo $row1['roll_no']; ?><br><?php  echo $row1['name']; ?></h2><hr>
				<ul>
					<li><b>Roll No. :</b> <?php echo $row1['roll_no']; ?></li>
					<li><b>Registration No. with session :</b> <?php echo $row1['reg_sess']; ?></li>
					<li><b>Full Name :</b> <?php echo $row1['name']; ?></li>
					<li><b>Academic Semester :</b> <?php echo $row1['semester']; ?></li>
					<li><b>Previoursly Earned Credit :</b> <?php echo $row1['earned_creadit']; ?></li>
					<li><b>Registration Date :</b> <?php echo $row1['birthday']; ?></li>
					<li><b>Backlock Courses :</b> <?php echo $row1['backlog']; ?></li>
				</ul>
				<h3 style="text-align: center;">Courses of this semester are:</h3>
				<table>
					<thead>
						<tr>
							<th>Course Number</th>
							<th>Course Title</th>
							<th>Course Credit</th>
						</tr>
					</thead>
					
					<tbody>
				<?php
				$sql = "SELECT * FROM courses_des WHERE roll_no = '". $row1['roll_no'] ."' AND semester = '". $row1['semester'] ."'";
				$result = mysqli_query($conn, $sql);
		
				if (mysqli_num_rows($result) > 0) {
					while ($row = mysqli_fetch_assoc($result)) { ?>
						
						<tr>
							<td><?php echo $row['course_no']; ?></td>
							<td><?php echo $row['course_title']; ?></td>
							<td><?php echo $row['course_credit']; ?></td>
						</tr>
				
					<?php
					}
				}
				?>
					</tbody>
							
				</table>
				<?php if($row1['active'] == 0){?>
					<form action="verify-course.php?val=<?php echo $row1['id']; ?>&val2=false" method="POST">
						<button type="submit" class="submit-button">Confirm</button>
					</form>
				<?php }else{ ?>
					<form action="verify-course.php?val=<?php echo $row1['id']; ?>&val2=true" method="POST">
						<button type="submit" class="submit-button">Cancel</button>
					</form>
				<?php } ?>
				
				</div>
			<?php }
		}
		?>

</body>
</html>
