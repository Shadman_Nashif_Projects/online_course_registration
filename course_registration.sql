-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 04:18 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course_registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(1500) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `active`) VALUES
(1, 'Shadman', '504a140afb8c2e7d1096738f2eadf9dd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `roll_no` varchar(8) NOT NULL,
  `reg_sess` varchar(50) NOT NULL,
  `name` varchar(500) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `earned_creadit` varchar(10) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `backlog` varchar(1500) NOT NULL,
  `recipte` varchar(1500) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `roll_no`, `reg_sess`, `name`, `semester`, `earned_creadit`, `birthday`, `backlog`, `recipte`, `active`) VALUES
(5, '123033', '395/2012-13', 'Shamim Hossain', '1st year 2nd semester', '20.50', '12-12-2014', 'Math, Chemistry', '', 0),
(6, '123015', '376/2012-13', 'MD. Shadman Nasif', '3rd year 4th semester', '100.5', '10/02/2016', 'Math, Chemistry', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses_des`
--

CREATE TABLE `courses_des` (
  `id` int(11) NOT NULL,
  `roll_no` varchar(8) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `course_no` varchar(50) NOT NULL,
  `course_title` varchar(100) NOT NULL,
  `course_credit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses_des`
--

INSERT INTO `courses_des` (`id`, `roll_no`, `semester`, `course_no`, `course_title`, `course_credit`) VALUES
(37, '123033', '1st year 2nd semester', 'CSE 200', 'Drawing and CAD Project', '0.75'),
(38, '123033', '1st year 2nd semester', 'CSE 201', 'Data structure', '3.00'),
(39, '123033', '1st year 2nd semester', 'CSE 202', 'Sessional based on CSE 201', '1.50'),
(40, '123033', '1st year 2nd semester', 'CSE 207', 'Object Oriented Programming', '3.00'),
(41, '123033', '1st year 2nd semester', 'CSE 208', 'Sessional based on CSE 207', '1.50'),
(42, '123033', '1st year 2nd semester', 'Ph 207', 'Physics', '3.00'),
(43, '123033', '1st year 2nd semester', 'Ph 208', 'Sessional based on Ph 207', '1.50'),
(44, '123033', '1st year 2nd semester', 'Math 207', 'Mathematics-II', '3.00'),
(45, '123033', '1st year 2nd semester', 'Hum 207', 'Economics, Government & Sociology', '4.00'),
(46, '123015', '3rd year 4th semester', '102', 'fgws', '1'),
(47, '123015', '3rd year 4th semester', '103', 'wdwhg', '2'),
(48, '123015', '3rd year 4th semester', '104', 'wdfwdfd', '3'),
(49, '123015', '3rd year 4th semester', '105', 'he3hekh', '4'),
(50, '123015', '3rd year 4th semester', '106', 'jekdjke', '2'),
(51, '123015', '3rd year 4th semester', '107', 'ckrfjrflr', '3'),
(52, '123015', '3rd year 4th semester', '108', 'kfj4r4', '4'),
(53, '123015', '3rd year 4th semester', '110', 'ejdrj4j', '2'),
(54, '123015', '3rd year 4th semester', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `structure`
--

CREATE TABLE `structure` (
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `structure`
--

INSERT INTO `structure` (`counter`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `registration` varchar(255) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `birthday` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `password` varchar(200) NOT NULL,
  `confirm_password` varchar(200) NOT NULL,
  `mobile_no` varchar(50) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `roll_no` varchar(10) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `time` varchar(20) NOT NULL,
  `picture` varchar(1500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `registration`, `firstname`, `lastname`, `birthday`, `gender`, `email`, `password`, `confirm_password`, `mobile_no`, `phone_no`, `roll_no`, `active`, `time`, `picture`) VALUES
(20, '001', 'somiron', 'pall', '1994-02-12', 'male', 'somiron_pall@gmail.com', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '01758635241', '8646846', '123050', 0, '2016/01/03 11:47:31p', ''),
(21, '002', 'shamim', 'hossain', '1994-01-01', 'male', 'hossains159@gmail.com', '202cb962ac59075b964b07152d234b70', '202cb962ac59075b964b07152d234b70', '01733412606', '46846846', '123033', 1, '2016/01/03 11:49:45p', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses_des`
--
ALTER TABLE `courses_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `courses_des`
--
ALTER TABLE `courses_des`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
