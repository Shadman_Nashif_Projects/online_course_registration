
<html>
<Head>
<link rel="stylesheet" type="text/css" href="signup_style.css">
</Head>
<body>
<div class="container">

    <form id="signup" action ="" method= "POST" style="padding-top: 30px;">

        <div class="header">
        
            <h3>Sign Up For First Course Registration</h3>
            
            <p><b>You must fill out this form below</b></p>
			<?php

include('init/db_connection.php');

$firstname = $lastname = $date_of_birth = $gender = "";
$password = $confirm_password = $email = $roll_no = $mobile_no = $phone_no = "";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['firstname']) === TRUE && !empty($_POST['firstname'])) {
        $firstname = $_POST['firstname'];
    }
	if (isset($_POST['lastname']) === TRUE && !empty($_POST['lastname'])) {
        $lastname = $_POST['lastname'];
    }
	if (isset($_POST['birthday']) === TRUE && !empty($_POST['birthday'])) {
        $date_of_birth = $_POST['birthday'];
    }
	if (isset($_POST['gender']) === TRUE && !empty($_POST['gender'])) {
        $gender = $_POST['gender'];
    }
	if (isset($_POST['email']) === TRUE && !empty($_POST['email'])) {
        $email = $_POST['email'];
    }
	if (isset($_POST['password']) === TRUE && !empty($_POST['password'])) {
        $password = md5($_POST['password']);
    }
	if (isset($_POST['confirm_password']) === TRUE && !empty($_POST['confirm_password'])) {
        $confirm_password = md5($_POST['confirm_password']);
    }
	if (isset($_POST['mobile_no']) === TRUE && !empty($_POST['mobile_no'])) {
        $mobile_no = $_POST['mobile_no'];
    }
	if (isset($_POST['phone_no']) === TRUE && !empty($_POST['phone_no'])) {
        $phone_no = $_POST['phone_no'];
    }
	if (isset($_POST['roll_no']) === TRUE && !empty($_POST['roll_no'])) {
        $roll_no = $_POST['roll_no'];
    }
	
	if($password == $confirm_password){
		date_default_timezone_set("Asia/Dhaka");
		$time = date("Y/m/d") .' '. date("h:i:sa");
		$sql = "SELECT counter FROM structure";
		$counter = mysqli_fetch_assoc(mysqli_query($conn, $sql));
		$count = $counter['counter']+1;
		$registration = "00". $count;
		$sql = "INSERT INTO student(registration, firstname, lastname, birthday, gender, email, password, confirm_password, mobile_no, phone_no, roll_no, time) VALUES('$registration', '$firstname', '$lastname', '$date_of_birth', '$gender', '$email', '$password', '$confirm_password', '$mobile_no', '$phone_no', '$roll_no', '$time')";
		if(mysqli_query($conn, $sql)){
			echo '<h4 class="success">Your account has been successfully created.</h4>';
			echo '<p style="color: blue;">You will recieve an email from us. Your account is under our verification process so please keep patient, this will not take more than two days.</p>';
			echo '<p style="color: blue;">Thank you very much for your co-operation.</p>';
			$sql = "UPDATE structure SET counter = " . $count;
			mysqli_query($conn, $sql);
		}else{
			echo '<h4 class="error">Something went wrong</h4>';
		}
	}else{
		echo '<h4 class="error">Password doesn\'t match</h4>';
	}
}

?>
            
        </div>
        
        <div class="sep"></div>

        <div class="inputs">
             
			 <input type="text" name="firstname" maxlength= "10" autofocus placeholder= "First name" required/>
             <input type="text" name="lastname" maxlength= "10" placeholder= "Last name"/> <br/>
            
            <input type="radio" name="gender" value="male" required/><i> Male </i>
            <input type="radio" name="gender" value="female" required/> <i>Female </i><br/><br/>
			 <input type="date" name="birthday" placeholder= "Birthdate" required> <br/>
			 
			<input type="email" name="email" placeholder="e-mail" autofocus required />
              <input type="password" name="password" placeholder="Password" required />
			  <input type="password" name="confirm_password" placeholder="Confirm Password" required />
			  <input type="tel" name="mobile_no" placeholder= "Mobile No." requird/>
			  <input type="tel" name="phone_no" placeholder= "Phone No." requird/>
			  <input type="text" name="roll_no" placeholder= "Roll_No." requird> <br/>
			  
            <!--
			<div class="checkboxy">
			     <i>Type Course Title & their Codes according to your choice:</i> <br/><br/>
				 <textarea style= "border: 1px solid navy; padding: 5px;" name="message" rows="6" cols="40">
				 </textarea><br/>
				 <i>Upload your image here: </i> <br/><br/>
				 <input type="file" name="img" multiple> <br/><br/>
                <input name="cecky" id="checky" value="1" type="checkbox" />
				<label class="terms">I accept the terms of use</label>
			-->
				<br><br>
				<input type="submit" style="margin-bottom: 20px; padding: 10px 20px;" value="Submit">
				<br><br>
            <br/>
        </div>

    </form>

</div>
</body>
</html>
