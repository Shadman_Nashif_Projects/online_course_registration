<?php
session_start();
include('init/db_connection.php');

if (isset($_SESSION['user_id']) === false && empty($_SESSION['user_id'])) {
    header('Location: index.html');
    exit();
}
if($_SESSION['user_id'] == 1){
	header('Location: admin.php');
}
$sql = "SELECT * FROM student WHERE id = ". $_SESSION['user_id'];
$data = mysqli_fetch_assoc(mysqli_query($conn, $sql));


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head profile="http://gmpg.org/xfn/11">
<title>User Profile</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
</head>
<body>
<div id="wrapp">
  <div id="header">
    <div id="HederTitle">
      <h1> <a href="index.html">Home</a> </h1>
      <span>USER PROFILE</span>
<a href="logout.php" style="color: #F4926E;">Sign Out</a>	  </div>
    <div class="clear"></div>
    <div id="BottomHeader">
      <div id="BottomHeaderWrapp"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
        <img src="<?php echo $data['picture']; ?>" align="left" height = "155" width= "150" alt=""/> <br />
        <p><?php echo ucwords($data['firstname']). ' '. ucwords($data['lastname']); ?></p>
        <p><i> Student at </i>Department of Computer Science & Engineering,RUET </p>
		<form action="fileupload.php" method="POST" enctype="multipart/form-data" style="padding-top: 30px;">
			<input type="file" name="profile"><br>
			<input type="submit" value="Upload">
		</form>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div id="wrappinner">
    <div id="main">
      <div class="entryBox" id="post-3">
        <div class="title">
          <h2> Welcome <?php echo ucwords($data['firstname']). ' '. ucwords($data['lastname']); ?> </h2>
        </div>
        <span class="date"><?php echo $data['time']; ?></span>
        <div class="entry">
          <p class="entry"><strong>My personal info goes here</strong> </p>
        </div>
      </div>
      <div class="entryBox" id="post-1">
        <div class="title">
          <h2>  <strong> Academic Result </strong> </h2>
        </div>
        
        <div class="entry">
          
        </div>
        
      </div>
      <div class="navigation">
        <div class="alignleft"></div>
        <div class="alignright"></div>
      </div>
    </div>
    <div id="sidebar">
      <ul>
	  <li>
        <h2>About</h2>
          </li>
		  <li><b>Full Name:</b>  <br><?php echo ucwords($data['firstname']). ' '. ucwords($data['lastname']); ?></li><br/>
		  <li><b>Bithdate:</b><br> <?php echo $data['birthday']; ?></li><br/>
		  <li> <b>Gender:</b><br> <?php echo $data['gender']; ?></li><br/>
		  <li><b>Email:</b><br> <?php echo $data['email']; ?></li><br/>
		  <li><b>Mobile No.:</b><br> <?php echo $data['mobile_no']; ?></li><br/>
		  <li><b>Phone No.:</b><br> <?php echo $data['phone_no']; ?></li><br/>
		  <li> <b>ID/Roll No:</b><br> <?php echo $data['roll_no']; ?></li><br/>		  
        
      </ul>
    </div>
    <div class="clear"></div>
	
	<!-- this is some of course section -->
	<div class="course-container">
		<h2>My Course Information Goes Here</h2>
	<?php
		$sql1 = "SELECT * FROM courses where roll_no = '" . $data['roll_no'] . "' and active=1";
		$result1 = mysqli_query($conn, $sql1);
		
		if (mysqli_num_rows($result1) > 0) {
			while ($row1 = mysqli_fetch_assoc($result1)) { ?>
			<div id="course-main-div">
			<br>
			<hr>
			<br>
				<ul style="list-style: none; line-height: 22px;">
					<li><b>Roll No. :</b> <?php echo $row1['roll_no']; ?></li>
					<li><b>Registration No. with session :</b> <?php echo $row1['reg_sess']; ?></li>
					<li><b>Full Name :</b> <?php echo $row1['name']; ?></li>
					<li><b>Academic Semester :</b> <?php echo $row1['semester']; ?></li>
					<li><b>Previoursly Earned Credit :</b> <?php echo $row1['earned_creadit']; ?></li>
					<li><b>Registration Date :</b> <?php echo $row1['birthday']; ?></li>
					<li><b>Backlock Courses :</b> <?php echo $row1['backlog']; ?></li>
				</ul>
				<br>
				<h3 style="text-align: center;">Courses of this semester are:</h3>
				<br>
				<table>
					<thead>
						<tr>
							<th class="center">Course Number</th>
							<th class="center">Course Title</th>
							<th class="center">Course Credit</th>
						</tr>
					</thead>
					
					<tbody>
				<?php
				$sql = "SELECT * FROM courses_des WHERE roll_no = '". $row1['roll_no'] ."' AND semester = '". $row1['semester'] ."'";
				$result = mysqli_query($conn, $sql);
		
				if (mysqli_num_rows($result) > 0) {
					while ($row = mysqli_fetch_assoc($result)) { ?>
						
						<tr>
							<td class="center"><?php echo $row['course_no']; ?></td>
							<td class="center"><?php echo $row['course_title']; ?></td>
							<td class="center"><?php echo $row['course_credit']; ?></td>
						</tr>
				
					<?php
					}
				}
				?>
					</tbody>		
				</table>
			</div>
			<br>
			<?php }
		}
		?>
  </div>

	<!-- this is some of end of course section -->
	
	
  <div class="clear"></div>
  <div id="footer">
    <p> Copyright@ <a href="www.ruet.ac.bd">ruet.ac.bd</a> </br>
      <span>Designed & Developed by Shadman Nashif Reju, sponsored by - <a href="http://www.6x3print.info/">6x3Print</a>. </span> </p>
  </div>
</div>
</body>
</html>
