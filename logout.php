<?php
session_start();
include('init/db_connection.php');

if (isset($_SESSION['user_id']) === false && empty($_SESSION['user_id'])) {
    header('Location: index.html');
    exit();
}

session_destroy();
header("Location: index.html");
?>